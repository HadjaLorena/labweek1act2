const express = require('express');
const app = express();
const homeRouter = require("./routes/home");
const middleware = require("./middleware/homePage")

app.get('/', (req, res) => {
  res.send('<h1> Hello, World! </h1>');
});

app.use(middleware.serverLogger);

app.use("/home", homeRouter);

app.listen(3000, () => {
  console.log(`Server is listening at http://localhost:3000`);
});